package org.jetbrains.dummy.lang

import org.junit.Test

class DummyLanguageTestGenerated : AbstractDummyLanguageTest() {
    @Test
    fun testAccessBeforeInit() {
        doTest("customTests\\accessBeforeInit.dummy")
    }
    
    @Test
    fun testAssignToVoid() {
        doTest("customTests\\assignToVoid.dummy")
    }
    
    @Test
    fun testBadReturn() {
        doTest("customTests\\badReturn.dummy")
    }
    
    @Test
    fun testParametersMismatch() {
        doTest("customTests\\parametersMismatch.dummy")
    }
    
    @Test
    fun testRedefinition() {
        doTest("customTests\\redefinition.dummy")
    }
    
    @Test
    fun testUninit() {
        doTest("customTests\\uninit.dummy")
    }
    
    @Test
    fun testUnknownFunction() {
        doTest("customTests\\unknownFunction.dummy")
    }
}
