/* General questions
 * 1) is it dynamically typed language or not? As far as we are going to develop for kotlin I guess no,
 *    so functions like { if (cond) return 1 else return; } are forbidden.
 *    (Boolean is some sort of int)
 * 2) no infinite recursion tracking. If functions explicitly returns something than it returns.
 * 3) scopes. possible to write:
 *       var a = 5;
 *       if (1) {
 *          a = 6;
 *          var a;
 *          a = 7;
 *       }
 *       // a == 6
 * 4) unreachable code
 * 5) wrong parameters count
 * 6) constant folding. I found out that there is no operations in language
 */

package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.*
import java.lang.IllegalStateException
import java.util.TreeMap
import java.util.HashMap

class VariableInitializationChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {
    override fun inspect(file: File) {
        file.accept(FileListener(), Unit)
    }

    // all error reporters
    private fun reportAccessBeforeInitialization(access: VariableAccess) {
        reporter.report(access, "Variable '${access.name}' is accessed before initialization")
    }
    private fun reportUnreachableCode(statement: Statement) {
        reporter.report(statement, "unreachable code")
    }
    private fun reportUndefinedReference(variableAccess: VariableAccess) {
        reporter.report(variableAccess, "Variable '${variableAccess.name}' undefined")
    }
    private fun reportRedefinition(variableDeclaration: VariableDeclaration) {
        reporter.report(variableDeclaration, "Variable '${variableDeclaration.name}' redefinition ignored")
    }
    private fun reportFunctionRedefinition(functionDeclaration: FunctionDeclaration) {
        reporter.report(functionDeclaration, "function '${functionDeclaration.name}' redefinition")
    }
    private fun reportUnknownFunction(functionCall: FunctionCall) {
        reporter.report(functionCall, "function '${functionCall.function}' was not declared")
    }
    private fun reportVoidFunction(functionCall: FunctionCall) {
        reporter.report(functionCall, "function '${functionCall.function}' returns void, but is used in expression")
    }
    private fun reportArgumentsMismatch(functionCall: FunctionCall) {
        reporter.report(functionCall, "function '${functionCall.function}' parameters mismatch")
    }
    private fun reportReturnTypesMismatch(statement: Statement) {
        reporter.report(statement, "return type differs from previously declared")
    }

    /**
     * Class to strore all variables declarations.
     * <p>
     * TODO:
     *   1) Avoid copying a map if all variables are set to true
     *   2) Cache results of searching in upper nodes
     *
     * @param upper link to upper scope in the list.
     */
    private inner class VariablesScope(
        val upper: VariablesScope?
    ) {
        val variables = TreeMap<String, Boolean>()

        /**
         * Cloning function.
         *
         * @returns deep copy
         */
        fun clone(): VariablesScope {
            val ret = VariablesScope(upper?.clone())
            variables.forEach { ret.variables[it.key] = it.value }
            return ret
        }

        /**
         * Sets name to value, if name exists. Otherwise if upper is not null,
         * calls this method for upper.
         *
         * @returns true, if something was assigned
         */
        private fun setNoAdd(name: String, value: Boolean) : Boolean {
            if (variables.containsKey(name)) {
                variables[name] = value
                return true
            } else if (upper == null) {
                return false
            }
            return upper.setNoAdd(name, value)
        }

        /**
         * Calls {@link setNoAdd}. If it returns false, creates new variable
         * in this scope with given name and value.
         */
        operator fun set(name: String, value: Boolean) {
            if (setNoAdd(name, value)) {
                return
            }
            variables[name] = value
        }

        /**
         * Assigns or declares in current scope.
         */
        fun putToCurrent(name: String, value: Boolean) = variables.put(name, value)

        /**
         * Checks if variable exists in current scope.
         */
        fun checkDeclaredInCurrent(variableDeclaration: VariableDeclaration) = variables.containsKey(variableDeclaration.name)

        /**
         * Checks that variable is initialized
         *
         * @returns null if not found, otherwise flag which is true only if variable
         *   was initialized in all execution paths.
         */
        operator fun get(name: String) : Boolean? {
            if (variables.containsKey(name)) {
                return variables[name]
            } else if (upper == null) {
                return null
            }
            return upper[name]
        }

        /**
         * Applies result of two branches to current.
         * All three must have same length, and contain same keys.
         * <p>
         * TODO:
         *   optimize merge speed.
         */
        fun merge(a: VariablesScope?, b: VariablesScope?) {
            if (a == null || b == null) {
                throw IllegalStateException("Kernel error during variable scopes merging: different chain sizes")
            }
            if (upper == null && (a.upper != null || b.upper != null)) {
                throw IllegalStateException("Kernel error during variable scopes merging: different chain sizes")
            }
            // merge current
            variables.forEach {
                if (variables[it.key] == true) {
                    return@forEach
                }
                val fromA = a.variables[it.key]
                val fromB = b.variables[it.key]
                if (fromA == null || fromB == null) {
                    throw IllegalStateException("Kernel error during variable scopes merging: different chain sizes")
                }
                variables[it.key] = fromA && fromB
            }

            // merge upper
            upper?.merge(a.upper, b.upper)
        }
    }

    /**
     * Data type to store all functions.
     *
     * @param functionDeclaration link to function declaration
     * @param hasReturn true if and only if all execution paths return something
     */
    private class FunctionHolder(
        val functionDeclaration: FunctionDeclaration,
        val hasReturn: Boolean
    ) {
        override fun equals(other: Any?) = functionDeclaration == other
        override fun hashCode() = functionDeclaration.hashCode()
    }

    /**
     * Container for {@link ReturnListener} class.
     *
     * @param returnStatement any return statement which matches current state (null if there were no return).
     * @param trustful equals true if all branches return. Used for unreachable code.
     */
    private data class ReturnInfo(
        val returnStatement: ReturnStatement?,
        val trustful: Boolean = true
    ) {
        companion object {
            val none = ReturnInfo(null, true)
        }
    }

    /**
     * Listener, which checks that return types match (all are void or not).
     */
    private inner class ReturnListener: DummyLangVisitor<ReturnInfo, Unit>() {
        override fun visitElement(element: Element, data: Unit): ReturnInfo = ReturnInfo.none

        override fun visitBlock(block: Block, data: Unit): ReturnInfo {
            var prev: ReturnInfo = ReturnInfo.none
            for (i in block.statements.indices) {
                val cur = block.statements[i].accept(this, Unit)
                if (cur.returnStatement != null) {
                    if (prev.returnStatement != null &&
                        (cur.returnStatement.result == null && prev.returnStatement!!.result != null ||
                                cur.returnStatement.result != null && prev.returnStatement!!.result == null)) {
                        reportReturnTypesMismatch(cur.returnStatement)
                    }
                    if (cur.trustful) {
                        if (block.statements.size - i - 1 != 0) {
                            reportUnreachableCode(block.statements[i + 1])
                        }
                        return cur
                    }
                    prev = cur
                }
            }
            // non void without return in the end
            if (block.statements.isNotEmpty() && prev.returnStatement != null && !prev.trustful && prev.returnStatement!!.result != null) {
                reportReturnTypesMismatch(block.statements.last())
            }
            return prev
        }

        override fun visitReturnStatement(returnStatement: ReturnStatement, data: Unit) = ReturnInfo(returnStatement)

        override fun visitFunctionDeclaration(functionDeclaration: FunctionDeclaration, data: Unit): ReturnInfo {
            return functionDeclaration.body.accept(this, Unit)
        }

        override fun visitIfStatement(ifStatement: IfStatement, data: Unit): ReturnInfo {
            val ifTrue = ifStatement.thenBlock.accept(this, Unit)
            if (ifStatement.elseBlock == null) {
                return ReturnInfo(ifTrue.returnStatement, false)
            }
            val ifFalse = ifStatement.elseBlock.accept(this, Unit)
            if (ifTrue.returnStatement == null) {
                return ReturnInfo(ifFalse.returnStatement, false)
            }
            if (ifFalse.returnStatement == null) {
                return ReturnInfo(ifTrue.returnStatement, false)
            }
            // both are not null
            if (ifTrue.returnStatement.result == null && ifFalse.returnStatement.result != null ||
                ifTrue.returnStatement.result != null && ifFalse.returnStatement.result == null) {
                reportReturnTypesMismatch(ifFalse.returnStatement)
            }
            return ReturnInfo(ifTrue.returnStatement, ifTrue.trustful && ifFalse.trustful)
        }
    }

    /**
     * Class which checks that during all execution paths accessed variable were initialized.
     * Doesn't check for excluding conditions, because language doesn't support
     *
     * @param functionDeclarations map from ReturnListener
     */
    private inner class VariableInitializationListener(
        val functionDeclarations: Map<String, FunctionHolder>
    ) : DummyLangVisitor<VariablesScope, VariablesScope>() {
        override fun visitElement(element: Element, data: VariablesScope): VariablesScope {
            return data
        }

        private fun checkCallIsValue(functionCall: FunctionCall) {
            val found = functionDeclarations[functionCall.function]
            if (found != null && !found.hasReturn) {
                reportVoidFunction(functionCall)
            }
        }

        override fun visitVariableDeclaration(variableDeclaration: VariableDeclaration, data: VariablesScope): VariablesScope {
            if (data.checkDeclaredInCurrent(variableDeclaration)) {
                reportRedefinition(variableDeclaration)
            }
            if (variableDeclaration.initializer != null) {
                variableDeclaration.initializer.accept(this, data)
                if (variableDeclaration.initializer is FunctionCall) {
                    checkCallIsValue(variableDeclaration.initializer)
                }
            }
            data.putToCurrent(variableDeclaration.name, variableDeclaration.initializer != null)
            return data
        }

        override fun visitAssignment(assignment: Assignment, data: VariablesScope): VariablesScope {
            assignment.rhs.accept(this, data)
            if (assignment.rhs is FunctionCall) {
                checkCallIsValue(assignment.rhs)
            }
            data[assignment.variable] = true
            return data
        }

        override fun visitBlock(block: Block, data: VariablesScope): VariablesScope {
            val newData = VariablesScope(data.clone())
            for (i in block.statements) {
                i.accept(this, newData)
            }
            return newData.upper!!
        }

        override fun visitFunctionCall(functionCall: FunctionCall, data: VariablesScope): VariablesScope {
            val found = functionDeclarations[functionCall.function]
            if (found == null) {
                reportUnknownFunction(functionCall)
            } else if (functionCall.arguments.size != found.functionDeclaration.parameters.size) {
                reportArgumentsMismatch(functionCall)
            }
            functionCall.arguments.forEach { it.accept(this, data) }
            return data
        }

        override fun visitVariableAccess(variableAccess: VariableAccess, data: VariablesScope): VariablesScope {
            val assigned = data[variableAccess.name]
            if (assigned == null) {
                reportUndefinedReference(variableAccess)
            } else if (assigned == false) {
                reportAccessBeforeInitialization(variableAccess)
            }
            return data
        }

        override fun visitIfStatement(ifStatement: IfStatement, data: VariablesScope): VariablesScope {
            ifStatement.condition.accept(this, data)
            val fromTrue = ifStatement.thenBlock.accept(this, data)
            if (ifStatement.elseBlock != null) {
                val fromFalse = ifStatement.elseBlock.accept(this, data)
                data.merge(fromTrue, fromFalse)
            }
            return data
        }

        // expects new VariablesScope
        override fun visitFunctionDeclaration(functionDeclaration: FunctionDeclaration, data: VariablesScope): VariablesScope {
            functionDeclaration.parameters.forEach { data[it] = true }
            functionDeclaration.body.accept(this, VariablesScope(data))
            return data
        }
    }

    /**
     * Accepts all functions with {@ling ReturnListener} and {@ling VariableInitializationListener}
     */
    private inner class FileListener: DummyLangVisitor<Unit, Unit>() {
        override fun visitElement(element: Element, data: Unit) = Unit

        override fun visitFile(file: File, data: Unit) {
            val functionsMap = HashMap<String, FunctionHolder>(file.functions.size)
            val retListener = ReturnListener()
            file.functions.forEach {
                val returnType = it.accept(retListener, Unit)
                if (functionsMap.putIfAbsent(it.name, FunctionHolder(it, returnType.returnStatement != null && returnType.trustful)) != null) {
                    reportFunctionRedefinition(it)
                }
            }
            val listener = VariableInitializationListener(functionsMap)
            file.functions.forEach { it.accept(listener, VariablesScope(null)) }
        }
    }
}
